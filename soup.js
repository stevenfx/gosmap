var s = Snap("#svg");
Snap.load("africas.svg", onSVGLoaded ) ;

function onSVGLoaded( data ){ 
    var elements = [];
    var hoverElem = data.select("#txt");
    hoverElem.attr({visibility:"hidden"});
    hoverElem.attr({fill:"#80112B"});
    function clicker(obj, id, info) {
	elements.push({id: id, obj: obj, hasColour: false});
	//Set default colour
	_.each(elements, function(e) {
	    if (e.obj != null && !e.hasColour) {
		e.obj.animate({'fill':'#0044AA'}, 200);
		e.hasColour = true; //We dont want to colour over and over.
	    }
	});

	obj.mouseover(function(e) {
	    hoverElem.attr({visibility:"visible"});
	    //hoverElem.attr({x:e.clientX, y:e.clientY});
	    $("#txt").text(info.name);
	});
	
	if (obj != null) {
	//Do click state
	obj.click(function() {
	    obj.animate({'fill':'#80112B'}, 200);
	    _.each(elements, function(e) {
		if (e.id == id)
		    return
		e.obj.animate({'fill':'#0044AA'}, 200);
	    });

	    //Clear
	    $("#map").removeClass();
	    $("#title").empty(); 
	    $("#location").empty(); 
	    $("#persons").empty(); 
	    $("#tel").empty(); 
	    $("#address").empty();
	    //Next Section
	    $("#map").addClass("map-"+id);
	    $("#title").html(info.title);
	    $("#location").html(info.location);

	    var persons = "<% _.each(persons, function(p) { %> <div class='title'><%= p.title %></div><div class='person'><span class='name'><%= p.name %></span> - <span class='email'><a href=''><%= p.email %></a></span></div> <% }); %>";
	    $("#persons").html(_.template(persons, {persons: info.persons}));
	    $("#tel").html(info.tel);
	    $("#address").html(info.address);

	});
	} else {
	    console.log("Does not exist " + id);
	}

    }
    //OK the rest of it...
    clicker(data.select("#za"), "za", { //South Africa
	flag: "za",
	name: "South Africa",
	title: "Global Outdoor Systems (PTY) Limited",
	location: "South Africa",
	persons: [
	    {
		title:"CEO:",
		name:"Vivienne Velthuis",
		email:"vivienne@gos.co.za"
	    },{
		title: "Group Sales & Marketing Director:",
		name: "Patrick de Robillard",
		email: "patrick@gos.co.za"
	    },{
		title: "International General Manager:",
		name: "Sylvain Rouch",
		email: "sylvain@gos.co.za"
	    },{
		title: "Financial Manager",
		name: "Christiaan Mulder",
		email: "christiaan@gos.co.za"
	    },{
		title: "Group Information and Market Research Director:",
		name: "Garth Dibb",
		email: "garth@gosafrica.com",
	    },{
		title: "Group Sales Manager:",
		name: "Andre Dantu",
		email: "andre@gosafrica.com",
	    },{
		title: "Technical Operations Manager:",
		name: "Jayton de Beer",
		email: "jayton@gosafrica.com",
	    },{
		title: "Regional Sales Manager:",
		name: "Pierre Nudlbichler",
		email: "pierre@gosafrica.com",
	    },{
		title: "Financial Manager:",
		name: "Larry Arries",
		email: "larry@gosafrica.com",
	    }
	]
    });
    clicker(data.select("#sn"), "sn", { //Senegal
	name: "Senegal",
	title: "Global Outdoor Systems",
	location: "Senegal",
	tel: "Mobile Number: (+221) 77 25 80 336<br>Tel: (+221) 772 580 336",
	address: "S/C Aspriale<br>150 Bis Route de Front de Terre<br>Dakar<br>Senegal",
	persons: [{
	    title: "General Manager",
	    name: "James Daykin",
	    email: "james@gos.co.za"
	}]
    }); 
    clicker(data.select("#gn"), "gn", { //Guinea
	name:"Guinea",
	title: "Global Outdoor Systems",
	location: "Guinea SA",
	address: "183 Rue KA 020<br>Immeuble Kaba<br>Boulbinet<br>B. P 3819<br>Conakry<br>GUINEA",
	tel: "Mobile: (+244) 6824 4573<br>Tel: (+244) 6950 6599",
	persons: [{
	    title: "General Manager",
	    name: "James Daykin",
	    email: "james@gos.co.za"
	}]
    }); 
    clicker(data.select("#ci"), "ci", { //Cote Divoire
	name: "Cote D'ivoire",
	title: "Global Outdoor Systems",
	location: "Cote D'ivoire SA",
	tel: "Mobile: <br>(+244) 05 85 3604<br>(+255) 21 34 0538<br>(+255) 21 34 0539<br>",
	address: "Groud Floor<br>Immeuble Les Elfes<br>Rue de Canal<br>Zone 4, Bietry<br>Abidjan",
	persons: [{
	    title: "General Manager",
	    name: "Joseph Lobbos",
	    email: ["joseph@gos.co.za","goscom.ci@afnet.net"]
	}, {
	    title: "Business Development Manager:",
	    name: "Claudia Nzimbou",
	    email: "claudia@gosafrica.com"
	}, {
	    title: "Financial Manager:",
	    name: "Davy Makosso Loumingou",
	    email: "gos.account.ci@afnet.net"
	}]
    }); 

    clicker(data.select("#gh"), "gh", { //Ghana
	name: "Ghana",
	title: "Global Outdoor Systems",
	location: "Ghana",
	tel: "Mobile: (+233) 24 4326861<br>Tel: (+233) 302 779940<br>Fax: (+233) 302 779942",
	address: "Plot 8 & 10<br>23 Second Close (off Volta Stree)<br>Airport Residential<br>Accra. Ghana",
	persons: [{
		title: "General Manager",
		name: "Shane Scott",
		email: "shane@gosafrica.com"
	}, {
	    title: "Business Development Manager:",
	    name: "Lucinda Ribeiro",
	    email: "lucinda@globalghana.com"
	}, {
	    title: "Financial Manager:",
	    name: "Frederick Anani Dogbatse",
	    email: "frederick@globalghana.com"
	}]
    }); 
    clicker(data.select("#bj"), "bj", { //Benin
	name: "Benin",
	title: "Global Outdoor Systems",
	location: "Benin Sarl",
	tel: "Mobile: (+255) 05 85 3604",
	persons: [{
	    title: "General Manager",
	    name: "Joseph Lobbos",
	    email: ["joseph@gos.co.za","goscom.ci@afnet.net"]
	}]
    }); 
    clicker(data.select("#ng"), "ng", { //Nigeria
	name: "Nigeria",
	title: "Global Outdoor Systems",
	location: "Nigeria",
	tel: "Mobile:<br>(+234) 803535 1950<br>(+234) 8057576263<br>(+234) 803 4023220",
	address: "10A Bayo Dejonwon Street, <br>Off Emmanuel Street, <br>Maryland, Ikeja, <br>Lagos<br>NIGERIA",
	persons: [{
		title: "General Manager:",
		name: "Dave Van Rensburg",
		email: "davevr@gosafrica.com",
	}, {
		title: "Sales Manager:",
		name: "Grant Fredericks",
		email: "grant@gos.co.za"
	}, {
		title: "Business Development Manager:",
		name: "Bello Yusuf",
		email: "bello@gosafrica.com"
	}, {
		title: "Financial Manager:",
		name: "Wayne Erlank",
		email: "wayne@gos.co.za"
	}]
    }); 
    clicker(data.select("#cm"), "cm", { //Cameroon
	name: "Cameroon",
	title: "Global Outdoor Systems",
	location: "Cameroon",
	address: "75 Rue de L'union Francaise<br>Immeuble Entrelec<br>Bali, Douala<br>Cameroon",
	tel: "Mobile: (+237) 775 02940/ (+237) 953 63 639<br>Tel: (+237) 334 39565<br>Fax: (+237) 334 27611",
	persons: [{
	    title:"General Manager:",
	    name: "Benjamin Simonis",
	    email: "benjamin@gosafrica.com"
	}, {
	    title: "Business Development Manager:",
	    name: "Eline Pech",
	    email: "sales-cameroon@gosafrica"
	},{
	    title: "Financial Manager:",
	    name: "Pierre Evina",
	    email: "finance-cameroon@gosafrica.com"
	}]
    }); 
    clicker(data.select("#ga"), "ga", { //Gabon
	name: "Gabon",
	title: "Global Outdoor Systems",
	location: "Gabon",
	address: "Avenue de Cointet<br>Residence Habiba, 1er etage<br>BP 1013<br>Libreville<br>GABON",
	tel: "Moblie: (+241) 078 886 43<br>(+241) 01 74 06 21",
	persons: [{
	    title: "General Manager",
	    name: "Mechael Houis",
	    email: "michael@gosafrica.com",
	}, {
	    title: "Business Development Manager:",
	    name: "Martine Zarblocki",
	    email: "martine@gosafrica.com"
	}]
    }); 
    clicker(data.select("#cg"), "cg", { //Congo
	name: "Congo",
	title: "Global Outdoor Systems",
	location: "Congo",
	tel: "Mobile: <br>(+242) 068 225 4601)<br>(+242) 065 084 393",
	persons: [{
	    title: "General Manager:",
	    name: "Prince Sedo",
	    email: "prince@gosafrica.com"
	}]
    }); 
    clicker(data.select("#cd"), "cd", { //DRC
	name: "DRC",
	title: "Global Outdoor Systems",
	location: "",
	tel: "Tel: +27(82) 499 4444",
	persons: [{
	    title: "International General Manager:",
	    name: "Sylvain Rouch",
	    email: "sylvain@gos.co.za"
	}]
    }); 
    clicker(data.select("#ao"), "ao", { //Angola
	name: "Angola",
	title: "Global Outdoor Systems (Angola)",
	location: "Angola",
	tel: "Mobile: (+244) 9361 98206",
	persons: [
	    {
		title: "General Manager:",
		name: "Wim Van Wyk",
		email: "wim@mediaexterioir.com"
	    }
	]
    }); 
    clicker(data.select("#tz"), "tz", { //Tanzania
	name: "Tanzania",
	title: "Global Outdoor Systems",
	location: "Tanzania",
	tel: "Mobile: (+244) 9361 98206",
	persons: [{
	    title: "General Manager:",
	    name: "Pierre Nudbichler",
	    email: "pierre@gosafrica.com"
	}]
    }); 
    clicker(data.select("#ke"), "ke", { //Kenya
	name: "Kenya",
	title: "Global Outdoor Systems",
	location: "Keyna",
	address: "Lenana Forest Centre<br> Plot 516 Dagoretti, Riruta<br>Ngong Road<br>Nairobi<br>Keyna",
	tel: "Mobile: (+ 254) 722741881/ (+ 254) 726373777<br>Tel: (+254) 20 2430361",
	persons: [{
	    title: "General Manager:",
	    name: "Medrine Kamau",
	    email: "medrine@globaloutdoorkenya.com"
	}]
    }); 
    clicker(data.select("#et"), "et", { //Ethiopia
	name: "Ethiopia",
	title: "Global Outdoor Systems",
	location: "",
	address: "No. 1197 Yeka Kefle Ketema Kevbele 17/18<br>Addis Adaba<br>Ethiopia",
	tel: "Mobile: (+255) 754939962",
	persons: [{
	    title: "General Manager:",
	    name: "Dave Howland",
	    email: "dave@globaloutdoorkenya.com"
	}]
    }); 

    clicker(data.select("#sd"), "sd", { //Sudan
	name: "Sudan",
	flag: "sd",
	title: "Global Outdoor Systems Sudan",
	location: "Sudan",
	tel: "",
	persons: [
	    {
		title: "General Manager",
		name: "Dave Howland",
		email: "dave@globaloutdoorkenya.com"
	    }
	]
    }); 
    clicker(data.select("#rw"), "rw", { //Rwanda
	name: "Rwanda",
	flag: "rw",
	title: "Global Outdoor Systems Rwanda",
	location: "Rwanda",
	persons: [
	    {
		title: "General Manager",
		name: "Dave Howland",
		email: "dave@globaloutdoorkenya.com"
	    }
	]
    }); 
    clicker(data.select("#zm"), "zm", { //Zambia
	name: "Zambia",
	flag: "zm",
	title: "Global Outdoor Systems Zambia",
	location: "Zambia",
	persons: [
	    {
		title: "General Manager",
		name: "Dave Howland",
		email: "dave@globaloutdoorkenya.com"
	    }
	]
    }); 
    clicker(data.select("#ug"), "ug", { //Uganda
	name: "Uganda",
	flag: "ug",
	title: "Global Outdoor Systems Uganda",
	location: "Uganda",
	persons: [
	    {
		title: "General Manager",
		name: "Dave Howland",
		email: "dave@globaloutdoorkenya.com"
	    }
	]
    }); 
    clicker(data.select("#zw"), "zw", { //Zimbabwe
	name: "Zumbabwe",
	flag: "za",
	title: "Global Outdoor Systems Zimbabwe",
	location: "Zimbabwe",
	persons: [
	    {
		title: "General Manager",
		name: "Dave Howland",
		email: "dave@globaloutdoorkenya.com"
	    }
	]
    });

    s.append(data);
}
